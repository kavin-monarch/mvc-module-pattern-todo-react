import React,{useState,useEffect} from 'react'
import Header from '../js/ViewComponents/Header/Header';
import ControllerModule from '../js/Controller/Controller.modules';
import Todo from '../js/ViewComponents/TodoContainer/Todo';

function App() {
    const [list,setList]=useState(ControllerModule.getList());
    return (
        <div>
            <Header remove={()=>ControllerModule.removeCompleteList} setList={setList}/>
            <Todo setList={setList} list={list}/>
        </div>  
    )
}
export default App
