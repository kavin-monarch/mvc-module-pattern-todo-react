import React from 'react'
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import ControllerModule from '../../Controller/Controller.modules';
function Header({setList}) {
    return (
        <>
        <div className="top_header">
             <h2>Todo List</h2>
            <div className="profile_header">
                <h2>Kavin</h2>
                <ExitToAppIcon 
                onClick={()=>{
                    ControllerModule.removeCompleteList();
                    setList(ControllerModule.getList());
                }}/>
            </div>
        </div>
        </>
    )
}
export default Header
