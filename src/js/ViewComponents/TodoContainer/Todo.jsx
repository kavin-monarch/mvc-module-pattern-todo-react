import React,{useState,useRef,useEffect} from 'react'
import ControllerModule from '../../Controller/Controller.modules';
import Todoitem from './Todoitem';
import SearchIcon from '@material-ui/icons/Search';
import ClearIcon from '@material-ui/icons/Clear';

function Todo({list,setList}) {

    const [input,setInput] = useState('');
    const [moveAndDrop, setMoveAndDrop] = useState({
        draggedX: null,
        draggedY: null,
        dragEnabled: false,
        previous: [],
        updated: []
    });
    const [search,setSearch]=useState(false);
    const [placehold,setPlaceHold]=useState('Enter your Task');
    const inputRef=useRef();

    useEffect(() => {
        !search || search?inputRef.current.focus():null
    }, [search])

    const searching=(v)=>{
        const result = ControllerModule.getList().filter((k) => {
            return k.text.toLowerCase().includes(v.toLowerCase());
        })
        // return result;
        setList(result);
    }

    //when dragging starts update state where to move the current dragging element
    const onStart = (event) => {
        const initialPosition = Number(event.currentTarget.dataset.position);
        
        setMoveAndDrop({
         ...moveAndDrop,
         draggedX: initialPosition,
         dragEnabled: true,
         previous: list
        });
       }

       //while dragging estimate the position of the draggable element
       const onEnter = (event) => {
  
        event.preventDefault();
        let newList = moveAndDrop.previous;
        const draggedX = moveAndDrop.draggedX; 
        const draggedY = Number(event.currentTarget.dataset.position); 
        const itemDragged = newList[draggedX];
        const remainingItems = newList.filter((item, index) => index !== draggedX);
         newList = [
          ...remainingItems.slice(0, draggedY),
          itemDragged,
          ...remainingItems.slice(draggedY)
         ];
        if (draggedY !== moveAndDrop.draggedY){
         setMoveAndDrop({
          ...moveAndDrop,
          updated: newList,
          draggedY: draggedY
         })
        }
        // console.log(moveAndDrop);
       }

       //update the element on estimated position 
       const onDrop = (event) => {
           if(!search){
            setList(moveAndDrop.updated);
            ControllerModule.updateModel(moveAndDrop.updated);
           }
        setMoveAndDrop({
         ...moveAndDrop,
         draggedX: null,
         draggedY: null,
         dragEnabled: false
        });
       }

       // when leave out of display container simply remove it from dragable and reset state
       const onLeave = () => {
         setMoveAndDrop({
         ...moveAndDrop,
         draggedY: null
        });
       }
       
        // add input user value to the model list
        const add=(e)=>{
            if(e.key==='Enter' && !search){
                if(e.target.value.length>0){
                    ControllerModule.addList(e.target.value,ControllerModule.idGen());
                    setInput('');
                }
                setList(ControllerModule.getList());
                // console.log(ControllerModule.getList());
            }
        }

        const input_change = (e)=>{
            console.log(e.target.value);
            setInput(e.target.value);
            if(e.target.value.length===0){
                setList(ControllerModule.getList());
            }
            if(search===true){
                searching(e.target.value);
            }
        }
        const toogle = (value)=>{
            if(value===1){
                setList(ControllerModule.getList());
                setInput('');
                setSearch(!search);
                setPlaceHold('Enter your Task');
            }
            else{
                setPlaceHold('Search');
                setSearch(!search);
            }
        }
    return (
           <div className="outer">
                   <div className="display__task">
                       <div className="input_search">
                       <input 
                        type ="text" 
                        placeholder={placehold} 
                        className="input" value={input} 
                        onChange={(e)=>input_change(e)} 
                        onKeyPress={e=>add(e)} 
                        autoFocus ref={inputRef}>
                        </input>
                        {
                            search===false?<SearchIcon onClick={()=>toogle()}/>:<ClearIcon onClick={()=>toogle(1)}/>
                        }
                       </div>
                   <hr/>
                       {   
                           list.map((task1,index)=>(
                               <div 
                               key={index}
                               data-position={index}
                               draggable
                               onDragStart={onStart}
                               onDragOver={onEnter}
                               onDrop={onDrop}
                               onDragLeave={onLeave}
                               >
                                    <Todoitem task={task1} search={search} inputRef={inputRef} list={list} setList={setList} />
                               </div>
                           ))
                       }
                   </div>
           </div>
    )
}

export default Todo
