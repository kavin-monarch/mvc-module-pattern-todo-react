import { Delete } from '@material-ui/icons'
import DoneIcon from '@material-ui/icons/Done';
import ClearIcon from '@material-ui/icons/Clear';
import EditIcon from '@material-ui/icons/Edit';
import React,{useState,useRef,useEffect} from 'react'
import ControllerModule from '../../Controller/Controller.modules';
function Todoitem({task,setList,list,inputRef,search}) {
    const [tick,setTick]=useState(task.status);
    const [editable,setEditable]=useState(false);
    const [tag,setTag]=useState(task.mark);
    const editableRef =useRef();

    useEffect(() => {
        editable && tick ?editableRef.current.focus():null;
        inputRef.current.focus();
        editable && tick ?atLast():null;
        setTag(task.mark);
        setTick(task.status);
    }, [editable,editableRef,setTag,task])

    // Event hadled for good user experience
    const caller =(e)=>{
        if(e.key==='Enter'){
            e.preventDefault();
            editableRef.current.blur();
            setEditable(!editable);
        }
        task.text=e.target.innerText;
    }
    // toogle editing feature or done feature
    const toogle =(value)=>{
        if(value===1){
            setTick(!task.status);
            task.status=!tick;
        }
        if(value===2){
            setEditable(!editable);
        }
    }

    // for current input position 
    const atLast=()=> {
        let input=editableRef.current;
        input.focus();
        if (typeof window.getSelection != "undefined"
            && typeof document.createRange != "undefined") {
            const range = document.createRange();
            range.selectNodeContents(input);
            range.collapse(false);
            const sel = window.getSelection();
            sel.removeAllRanges();
            sel.addRange(range);
        }
    }

    //change tag color 
    const setTaged=(value)=>{
        // if(!search){
            task.mark=value;
            setTag(value);
            setList(list);
            setEditable(false);
        // }
    }
    return (
        <div className="content__task" id={tag}>
            <div className="content__name">
                {
                     tick?
                    <div 
                     id="input" 
                     tabIndex={-1} 
                     ref={editableRef} 
                     contentEditable={editable} 
                     suppressContentEditableWarning={editable}  
                     onKeyPress={(e)=>{caller(e)}}>{task.text}
                    </div>:
                     <div  className="strike">{task.text} </div>
                }
            </div>
            <div className="content__delete">
                <div className="TagMain">
                    <div id={"Tag1"} className={"Tag"} onClick={()=>setTaged('Tag'+1)}>&zwnj;</div>
                    <div id={"Tag2"} className={"Tag"} onClick={()=>setTaged('Tag'+2)}>&zwnj;</div>
                    <div id={"Tag3"} className={"Tag"} onClick={()=>setTaged('Tag'+3)}>&zwnj;</div>
                    <div id={"Tag4"} className={"Tag"} onClick={()=>setTaged('Tag'+4)}>&zwnj;</div>
                </div>
                {tick?<DoneIcon onClick={()=>toogle(1)}/>:<ClearIcon onClick={()=>toogle(1)}/>}
                {<EditIcon onClick={()=>toogle(2)}/>}
              <Delete 
              onClick={()=>{
                  ControllerModule.removeParticular(task.id);
                  setList(ControllerModule.getList());
                  }}/>
            </div>
        </div>
    )
}

export default Todoitem
