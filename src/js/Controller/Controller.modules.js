import model from '../Model/Model';
function User(id,text){
    this.id=id;
    this.text=text;
    this.status=true;
    this.mark='Tag1';
}
const ControllerModule={
    idGen:()=>{
        var id = Math.random() * 16
        id=id.toString(16);
        return id;
    },
    addList:(value,id)=>{
             let obj =new User(id,value);
             let arr=model.getList();
             arr.push(obj);
             model.updateModel(arr);
    },
    removeCompleteList:()=>{
        model.removeCompleteList();

    },
    updateModel:(updated)=>{
        model.updateModel(updated);
    },
    getList:()=>{
        return model.getList();
    },
    removeParticular:(id)=>{
        let arr=model.getList().filter(task=>{
            if(task.id!==id)
                return task;
            return '';
        })
        model.updateModel(arr);
    }
}
export default ControllerModule;