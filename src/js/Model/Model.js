let model =(()=>{
    let Lists=[];
    const removeCompleteList=()=>{
        Lists=[];
    }
    const updateModel=(updated)=>{
        Lists=updated;
    };
    let getList=()=>{
        return Lists;
    }
    return{
        removeCompleteList:removeCompleteList,
        updateModel:updateModel,
        getList:getList
    }
})();

export default model;